﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TweenAlpha : MonoBehaviour {
	float speed = 1.0f;
	Image image;
	// Use this for initialization
	void Start () {
		image = gameObject.GetComponent<Image>();
	}
	
	// Update is called once per frame
	void Update () {
		if (image.color.a <= 0 || image.color.a >= 1)
			speed = -speed;
		Color color = image.color;
		color.a = color.a + Time.deltaTime * speed;
		image.color = color;
	}
}
