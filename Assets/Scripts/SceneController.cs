﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;
using Unity.Jobs;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.Events;
using Unity.Collections;
using GoogleMobileAds.Api;


public class SceneController : MonoBehaviour
{
	private enum CallType { Numbers, Animals, Colors };
	private enum CallPhase { Dial, Call, CallUser, IncomingDial };
	public GameObject[] buttons;

	[SerializeField] private GameObject inputPanel, callPanel;
	[SerializeField] private Image animalPortrait, animalBG;
	[SerializeField] private GameObject colorButtonPrefab, numberButtonPrefab, animalButtonPrefab;
	[SerializeField] private GameObject callButtonPrefab, recordButtonPrefab;
	[SerializeField] private GameObject leftSwitchButton, rightSwitchButton;
	[SerializeField] private GameObject buttonsPanel;
	[SerializeField] private Text inputField, callInput;
	[SerializeField] private GameObject shadow, phone;
	[SerializeField] private CallTime callTime;
	[SerializeField] private GameObject recordPanel;
	[SerializeField] private Text timerLabel, recordLabel;
	[SerializeField] private GameObject cancelButton, callButtonsPanel;
	[SerializeField] private RotateTween answerRotate;
	[SerializeField] private Animator callButtonAnimator;
	[SerializeField] private Button callButton, recordButton;

	System.Random rand;

	private Dictionary<string, string> strings;
	private Dictionary<string, Color> colors;
	private string[] animals;
	private GameObject prevButton;
	private string currAnimal = "raccoon", callAnimal = "raccoon";
	private CallType callType = CallType.Numbers;
	private bool isInCall = false, isInRecord = false, wasIncoming = false;
	private AudioSource audioSource;
	private CallPhase callPhase = CallPhase.Dial;
	private AudioClip userClip = null;
	int wait = 0;
	private string inputText = "", callInputText = "";
	private Coroutine microphoneCoroutine = null;

	private static float buttonXPercentage = 0.24f;
	private static float buttonYPercentage = 0.24f;
	private static float spacingXPercentage = (1f - buttonXPercentage * 3) / 2f;
	private static float spacingYPercentage = (1f - buttonYPercentage * 4) / 3f;


	public void OnNumberButtonClick()
	{
		UserButton button = EventSystem.current.currentSelectedGameObject.
			GetComponent<UserButton>();

		if (inputText.Length >= 6)
			inputText = button.Number.ToString();
		else
			inputText = inputText + button.Number;

		if (inputText.Length == 0)
			currAnimal = animals[0];
		else
		{
			int currNumber = Convert.ToInt32(inputText.Substring(0, 1));
			currAnimal = animals[currNumber];
		}

		Helper.PlayAudio("number_" + button.Number, audioSource);
	}


	public void OnColorButtonClick()
	{
		UserButton button = EventSystem.current.currentSelectedGameObject.
			GetComponent<UserButton>();
		inputText = strings[button.Color];
		inputField.color = colors[button.Color];

		Helper.PlayAudio(button.Color, audioSource);
	}



	public void OnAnimalButtonClick()
	{
		if (prevButton != null)
			prevButton.GetComponent<UserButton>().Active = false;
		GameObject currButton = EventSystem.current.currentSelectedGameObject;
		currButton.GetComponent<UserButton>().Active = true;
		prevButton = currButton;

		UserButton button = currButton.GetComponent<UserButton>();
		inputText = strings[button.Animal];
		currAnimal = button.Animal;

		//callButtonAnimator.Play("CallButtonAnimation", 0);
		callButtonAnimator.Rebind();
		callButtonAnimator.SetTrigger("PlayTrigger");
		//callButtonAnimator.Play("CallButtonAnimation");

		Helper.PlayAudio(button.Animal, audioSource);
	}

	private void SetupSwitches(string leftSprite, string rightSprite, UnityAction leftCall, UnityAction rightCall)
	{
		leftSwitchButton.GetComponent<Image>().sprite = Resources.Load<Sprite>("Switches/" + leftSprite);
		Button leftButton = leftSwitchButton.GetComponent<Button>();
		leftButton.onClick.RemoveAllListeners();
		leftButton.onClick.AddListener(leftCall);

		rightSwitchButton.GetComponent<Image>().sprite = Resources.Load<Sprite>("Switches/" + rightSprite);
		Button rightButton = rightSwitchButton.GetComponent<Button>();
		rightButton.onClick.RemoveAllListeners();
		rightButton.onClick.AddListener(rightCall);
	}

	private void OnNumbersButtonClick()
	{
		currAnimal = animals[0];
		inputField.color = Helper.DefaultInputColor;
		inputText = "";
		SetupSwitches("ic_colors_button", "ic_animals_button", OnColorsButtonClick, OnAnimalsButtonClick);
		callType = CallType.Numbers;

		for (int i = 0; i < buttons.Length; i++)
		{
			buttons[i].GetComponent<UserButton>().ButtonMode = UserButton.Mode.Number;
			buttons[i].GetComponent<UserButton>().Active = false;
			Button button = buttons[i].GetComponent<Button>();
			button.onClick.RemoveAllListeners();
			button.onClick.AddListener(OnNumberButtonClick);
		}

		callButton.image.sprite = Resources.Load<Sprite>("Sprites/ic_call_button");
		recordButton.image.sprite = Resources.Load<Sprite>("Sprites/ic_dictaphone_button");
	}

	private void OnColorsButtonClick()
	{
		inputText = "";
		SetupSwitches("ic_animals_button", "ic_numbers_button", OnAnimalsButtonClick, OnNumbersButtonClick);
		callType = CallType.Colors;

		for (int i = 0; i < buttons.Length; i++)
		{
			buttons[i].GetComponent<UserButton>().ButtonMode = UserButton.Mode.Color;
			buttons[i].GetComponent<UserButton>().Active = false;
			Button button = buttons[i].GetComponent<Button>();
			button.onClick.RemoveAllListeners();
			button.onClick.AddListener(OnColorButtonClick);
		}

		callButton.image.sprite = Resources.Load<Sprite>("Colors/ic_call_button");
		recordButton.image.sprite = Resources.Load<Sprite>("Colors/ic_dictaphone_button");
	}

	private void OnAnimalsButtonClick()
	{
		currAnimal = animals[1];
		inputField.color = Helper.DefaultInputColor;
		inputText = "";
		SetupSwitches("ic_numbers_button", "ic_colors_button", OnNumbersButtonClick, OnColorsButtonClick);
		callType = CallType.Animals;

		for (int i = 0; i < buttons.Length; i++)
		{
			buttons[i].GetComponent<UserButton>().ButtonMode = UserButton.Mode.Animal;
			buttons[i].GetComponent<UserButton>().Active = false;
			Button button = buttons[i].GetComponent<Button>();
			button.onClick.RemoveAllListeners();
			button.onClick.AddListener(OnAnimalButtonClick);
		}

		callButton.image.sprite = Resources.Load<Sprite>("Sprites/ic_call_button");
		recordButton.image.sprite = Resources.Load<Sprite>("Sprites/ic_dictaphone_button");
	}

	public void OnCallButtonClick()
	{
		callAnimal = currAnimal;
		if (callType == CallType.Colors)
		{
			int idx = rand.Next(0, animals.Length);
			currAnimal = animals[idx];
		}
		callInputText = strings["outgoing_call"];
		animalPortrait.gameObject.SetActive(true);
		animalPortrait.sprite = Resources.Load<Sprite>("BigAnimals/" + callAnimal + "_big");
		animalBG.sprite = Resources.Load<Sprite>("AnimalBG/background_" + callAnimal);
		callButtonAnimator.Rebind();
		inputPanel.SetActive(false);
		callPanel.SetActive(true);
		callPhase = CallPhase.Dial;
		isInCall = true;
		Helper.PlayAudio("dial", audioSource, false);
	}

	private void StartRecordCall()
	{
		recordPanel.SetActive(false);
		callAnimal = "user";
		callInputText = strings["outgoing_call"];
		animalPortrait.gameObject.SetActive(false);
		animalBG.sprite = Resources.Load<Sprite>("Sprites/background_user");
		callButtonAnimator.Rebind();
		inputPanel.SetActive(false);
		callPanel.SetActive(true);
		callPhase = CallPhase.Dial;
		isInCall = true;
		Helper.PlayAudio("dial", audioSource, false);
	}

	IEnumerator MicrophoneTimerCorutine()
	{
		for (int i = 9; i >= 0; i--)
		{
			timerLabel.text = i.ToString();
			yield return new WaitForSeconds(1);
		}

		isInRecord = false;
		StartRecordCall();
	}

	public void OnRecordButtonClick()
	{
		isInRecord = true;
		userClip = Microphone.Start(null, false, 10, 44100);
		recordLabel.text = strings["voice_recording"];
		microphoneCoroutine = StartCoroutine(MicrophoneTimerCorutine());
		recordPanel.SetActive(true);
	}

	private void StopCall()
	{
		isInCall = false;
		inputPanel.SetActive(true);
		callPanel.SetActive(false);
		shadow.SetActive(true);
		callButtonsPanel.SetActive(false);
		cancelButton.SetActive(true);
		phone.SetActive(true);
		audioSource.Stop();
		callTime.StopTimer();
		if (wasIncoming)
			StartCoroutine(IncomingCallCorutine());
		wasIncoming = false;


	}

	public void OnStopRecordButtonClick()
	{
		StopCoroutine(microphoneCoroutine);
		isInRecord = false;
		recordPanel.SetActive(false);
		inputPanel.SetActive(true);
	}

	public void OnCancelButtonClick()
	{
		StopCall();
	}

	private IEnumerator IncomingCallCorutine()
	{
		while (true)
		{
			Debug.Log("wait " + wait);
			yield return new WaitForSeconds(wait);
			if (!isInRecord && !isInCall)
			{

				int idx = rand.Next(0, animals.Length);
				callAnimal = animals[idx];
				callInputText = strings[callAnimal];
				animalPortrait.gameObject.SetActive(true);
				animalPortrait.sprite = Resources.Load<Sprite>("BigAnimals/" + callAnimal + "_big");
				shadow.SetActive(false);
				animalBG.sprite = Resources.Load<Sprite>("AnimalBG/background_" + callAnimal);
				callButtonAnimator.Rebind();
				inputPanel.SetActive(false);
				phone.SetActive(false);
				callPanel.SetActive(true);
				callPhase = CallPhase.IncomingDial;
				cancelButton.SetActive(false);
				callButtonsPanel.SetActive(true);
				wasIncoming = true;
				answerRotate.StartRotate();
				Helper.PlayAudio("incoming_sound", audioSource, false);
				isInCall = true;
				break;
			}
		}
	}

	public void OnAnswerButtonClick()
	{
		callButtonsPanel.SetActive(false);
		cancelButton.SetActive(true);
		answerRotate.StopRotate();
		wait += 40;
		audioSource.Stop();
		callPhase = CallPhase.Call;
		callTime.StartTimer();
	}

	// Use this for initialization
	void Start()
	{
		Screen.orientation = ScreenOrientation.Portrait;

		if (Helper.GetLangCode() == "vi")
		{
			callInput.font = Resources.Load<Font>("Fonts/PaytoneOne-Regular");
			recordLabel.font = Resources.Load<Font>("Fonts/PaytoneOne-Regular");
			inputField.font = Resources.Load<Font>("Fonts/PaytoneOne-Regular");
		}
		else
		{
			callInput.font = Resources.Load<Font>("Fonts/Muller");
			recordLabel.font = Resources.Load<Font>("Fonts/Muller");
			inputField.font = Resources.Load<Font>("Fonts/Muller");
		}

		leftSwitchButton.GetComponent<Button>().onClick.AddListener(OnColorsButtonClick);
		rightSwitchButton.GetComponent<Button>().onClick.AddListener(OnAnimalsButtonClick);
		strings = Helper.LoadStrings();
		colors = Helper.LoadColors();
		animals = Helper.LoadAnimals();
		audioSource = this.gameObject.GetComponent<AudioSource>();
		rand = new System.Random();
		//wait = rand.Next(4000, 5000);
		wait = rand.Next(50, 81);
		//	wait = rand.Next(10, 15);

		TextAsset colorListText = Resources.Load<TextAsset>("color_list");
		ColorList colorList = JsonUtility.FromJson<ColorList>(colorListText.text);

		for (int i = 0; i < buttons.Length; i++)
		{
			buttons[i].GetComponent<UserButton>().Number = i;
			buttons[i].GetComponent<UserButton>().Color = colorList.keys[i];
			buttons[i].GetComponent<UserButton>().Animal = animals[i];
			Button button = buttons[i].GetComponent<Button>();
			button.onClick.RemoveAllListeners();
			button.onClick.AddListener(OnNumberButtonClick);
		}

#if UNITY_ANDROID
		string appId = "ca-app-pub-3940256099942544~3347511713";
#elif UNITY_IOS
            string appId = "ca-app-pub-3940256099942544~1458002511";
#else
            string appId = "unexpected_platform";
#endif

		// Initialize the Google Mobile Ads SDK.
		MobileAds.Initialize(appId);

		StartCoroutine(IncomingCallCorutine());
	}

	// Update is called once per frame
	void Update()
	{
		inputField.text = inputText;
		callInput.text = callInputText;

		if (isInCall)
		{
			if (callPhase == CallPhase.Dial)
			{
				if (!audioSource.isPlaying)
				{
					shadow.SetActive(false);
					phone.SetActive(false);
					callPhase = CallPhase.Call;

					if (callAnimal != "user")
						callInputText = strings[callAnimal];
					else
						callInputText = "";

					callTime.StartTimer();
				}
			}
			else if (callPhase == CallPhase.CallUser || callPhase == CallPhase.Call)
			{
				if (callAnimal != "user")
				{
					if (!audioSource.isPlaying)
					{
						int idx = rand.Next(1, 4);
						Helper.PlayAudio(callAnimal + "_" + idx, audioSource, false);
					}
				}
				else
				{
					if (callPhase != CallPhase.CallUser)
					{
						if (!audioSource.isPlaying)
						{
							callPhase = CallPhase.CallUser;
							audioSource.PlayOneShot(userClip);
						}
					}
					else if (!audioSource.isPlaying)
						StopCall();
				}
			}
			else if (callPhase == CallPhase.IncomingDial)
			{
				if (!audioSource.isPlaying)
					StopCall();
			}
		}

	}
}
