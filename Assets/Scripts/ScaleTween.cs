﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScaleTween : MonoBehaviour {
	public float speedX = 0.01f, speedY = 0.01f, speedZ = 0;
	public float deltaX = 0.1f, deltaY = 0.1f, deltaZ = 1.0f;
	float defaultX, defaultY, defaultZ;
	float currX, currY, currZ;
	bool rotate = true;

	// Use this for initialization
	void Start()
	{
		defaultX = gameObject.transform.localScale.x;
		defaultY = gameObject.transform.localScale.y;
		defaultZ = gameObject.transform.localScale.z;
		currX = defaultX;
		currY = defaultY;
		currZ = defaultZ;
	}

	public void StartRotate()
	{
		currX = defaultX;
		currY = defaultY;
		currZ = defaultZ;
		gameObject.transform.localScale = new Vector3(defaultX, defaultY, defaultZ);
		rotate = true;
	}

	public void StopRotate()
	{
		currX = defaultX;
		currY = defaultY;
		currZ = defaultZ;
		rotate = false;
		gameObject.transform.localScale = new Vector3(defaultX, defaultY, defaultZ);
	}

	// Update is called once per frame
	void Update()
	{
		if (rotate)
		{
			currX = currX + Time.deltaTime * speedX;
			currY = currY + Time.deltaTime * speedY;
			currZ = currZ + Time.deltaTime * speedZ;

			currX = Mathf.Clamp(currX, defaultX - deltaX, defaultX + deltaX);
			currY = Mathf.Clamp(currY, defaultY - deltaY, defaultY + deltaY);
			currZ = Mathf.Clamp(currZ, defaultZ - deltaZ, defaultZ + deltaZ);
			if (deltaX <= Mathf.Abs(defaultX - currX))
				speedX = -speedX;
			if (deltaY <= Mathf.Abs(defaultY - currY))
				speedY = -speedY;
			if (deltaZ <= Mathf.Abs(defaultZ - currZ))
				speedZ = -speedZ;

			gameObject.transform.localScale = new Vector3(currX, currY, currZ);
		}
	}
}
