﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public static class Helper
{
	public static Color DefaultInputColor = new Color(6.0f / 255.0f, 108.0f / 255.0f, 170.0f / 255.0f);
	public static string GetLangCode()
	{
		SystemLanguage lang = Application.systemLanguage;
		//SystemLanguage lang = SystemLanguage.Vietnamese;
		switch (lang)
		{
			case SystemLanguage.Arabic:
				return "ar";
			case SystemLanguage.German:
				return "de";
			case SystemLanguage.English:
				return "en";
			case SystemLanguage.Spanish:
				return "es";
			case SystemLanguage.French:
				return "fr";
			case SystemLanguage.Indonesian:
				return "ind";
			case SystemLanguage.Italian:
				return "it";
			case SystemLanguage.Portuguese:
				return "pt";
			case SystemLanguage.Russian:
			case SystemLanguage.Ukrainian:
				return "ru";
			case SystemLanguage.Thai:
				return "th";
			case SystemLanguage.Turkish:
				return "tr";
			case SystemLanguage.Vietnamese:
				return "vi";
			default:
				return "en";
		}
	}

	[Serializable]
	private class ConfigStringsArray
	{
		public ConfigString[] items;
	}

	[Serializable]
	private class ConfigString
	{
		public string key, value;
	}

	private static string ReverseString(string inp)
	{
		string result = "";
		for (int i = inp.Length - 1; i >= 0; i--)
			result += inp[i];

		return result;
	}

	public static Dictionary<string, string> LoadStrings()
	{
		String langCode = Helper.GetLangCode();
		Dictionary<string, string> strings = new Dictionary<string, string>();
		string path = string.Format("Strings/{0}/strings", langCode);
		TextAsset stringsText = Resources.Load<TextAsset>(path);
		ConfigStringsArray stringsJSON = JsonUtility.FromJson<ConfigStringsArray>(stringsText.text);
		foreach (ConfigString configString in stringsJSON.items)
			strings.Add(configString.key, configString.value);

		if (langCode == "ar")
		{
			Dictionary<String, String> strings2 = new Dictionary<string, string>();
			foreach (String key in strings.Keys)
			{
				string str = ReverseString(strings[key]);
				strings2.Add(key, str);
			}

			strings = strings2;
		}

		return strings;
	}

	[Serializable]
	private class ConfigColorsArray
	{
		public ConfigKeyValueColor[] items;
	}

	[Serializable]
	private class ConfigKeyValueColor
	{
		public string name;
		public ConfigColor color;
	}

	[Serializable]
	private class ConfigColor
	{
		public float r, g, b;
	}

	public static Dictionary<string, Color> LoadColors()
	{
		Dictionary<string, Color> colors = new Dictionary<string, Color>();
		TextAsset colorsText = Resources.Load<TextAsset>("color_values");
		ConfigColorsArray colorsJSON = JsonUtility.FromJson<ConfigColorsArray>(colorsText.text);
		foreach (ConfigKeyValueColor configKeyValueColor in colorsJSON.items)
		{
			ConfigColor configColor = configKeyValueColor.color;
			colors.Add(configKeyValueColor.name, new Color(configColor.r / 255.0f, configColor.g / 255.0f, configColor.b / 255.0f));
		}

		return colors;
	}

	[Serializable]
	private class JsonStringsArray
	{
		public string[] items;
	}

	public static string[] LoadAnimals()
	{
		TextAsset animalsText = Resources.Load<TextAsset>("animals_list");
		JsonStringsArray animalsJSON = JsonUtility.FromJson<JsonStringsArray>(animalsText.text);
		return animalsJSON.items;
	}

	public static void PlayAudio(string name, AudioSource source)
	{
		PlayAudio(name, source, true);
	}

	public static void PlayAudio(string name, AudioSource source, bool useLang)
	{
		string path;
		if (useLang)
			path = string.Format("Audio/{0}/{1}", GetLangCode(), name);
		else
			path = string.Format("Audio/common/{0}", name);

		AudioClip clip = Resources.Load<AudioClip>(path);
		if (source.isPlaying)
			source.Stop();

		source.PlayOneShot(clip);
	}
}
