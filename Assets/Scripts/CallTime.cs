﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CallTime : MonoBehaviour {
	bool timerWork = false;

	int elapsed = 0;
	// Use this for initialization
	void Start () {
	}

	public void StartTimer()
	{
		elapsed = 0;
		timerWork = true;
		gameObject.SetActive(true);
		StartCoroutine(TimerCorutine());
	}

	public void StopTimer()
	{
		timerWork = false;
		gameObject.SetActive(false);
	}

	private IEnumerator TimerCorutine()
	{
		Text callTime;
		callTime = gameObject.GetComponent<Text>();
		while (timerWork)
		{
			callTime.text = string.Format("{0:D2}:{1:D2}", elapsed / 60, elapsed % 60);
			elapsed += 1;
			elapsed %= 6000;
			yield return new WaitForSeconds(1);
		}
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
