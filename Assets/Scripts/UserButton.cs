﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UserButton : MonoBehaviour {
	private int nmb = 0;
	private string color = "red";
	private string animal = "raccoon";
	public enum Mode { Number, Color, Animal };
	bool active = false;


	private Mode mode = Mode.Number;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (mode == Mode.Number)
		{
			this.gameObject.transform.GetChild(0).
				gameObject.GetComponent<Text>().text = nmb.ToString();
			this.gameObject.GetComponent<Button>().image.sprite = 
				Resources.Load<Sprite>("Sprites/ic_number_button");
			this.gameObject.transform.GetChild(1).
				gameObject.SetActive(false);
		}
		else if (mode == Mode.Color)
		{
			this.gameObject.transform.GetChild(0).
				gameObject.GetComponent<Text>().text = "";
			Button button = this.gameObject.GetComponent<Button>();
			button.image.sprite = Resources.Load<Sprite>("Colors/ic_" + color + "_button");
			this.gameObject.transform.GetChild(1).
				gameObject.SetActive(false);
		}
		else if (mode == Mode.Animal)
		{
			this.gameObject.transform.GetChild(0).
				gameObject.GetComponent<Text>().text = "";
			GameObject imageObject = this.gameObject.transform.GetChild(1).gameObject;
			imageObject.SetActive(true);
			if (!active)
				this.gameObject.GetComponent<Button>().image.sprite =
					Resources.Load<Sprite>("Animals/ic_animal_button");
			else
				this.gameObject.GetComponent<Button>().image.sprite =
					Resources.Load<Sprite>("Animals/ic_animal_selected_button");
			imageObject.GetComponent<Image>().sprite = Resources.Load<Sprite>("Animals/" + animal);
		}
	}

	public int Number {
		set {
			this.nmb = value;
		}

		get { return nmb; }
	}

	public string Color
	{
		set
		{
			this.color = value;
		}

		get { return color; }
	}

	public string Animal
	{
		set
		{
			this.animal = value;
		}

		get { return animal; }
	}

	public Mode ButtonMode
	{
		set { mode = value; }
	}
	
	public bool Active
	{
		get
		{
			return active;
		}

		set { active = value; }
	}
}
