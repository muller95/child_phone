﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class ColorButtonSpawner : MonoBehaviour
{
	[SerializeField] private GameObject buttonPrefab, recordButtonPrefab, callButtonPrefab;
	[SerializeField] private InputField inputField;

	private Dictionary<string, string> strings;
	private Dictionary<string, Color> colors;

	// Use this for initialization
	void Start()
	{
		TextAsset colorListText = Resources.Load<TextAsset>("color_list");
		ColorList colorList = JsonUtility.FromJson<ColorList>(colorListText.text);
		strings = Helper.LoadStrings();
		colors = Helper.LoadColors();

		float buttonXPercentage = 0.28f;
		float buttonYPercentage = 0.24f;
		float spacingXPercentage = (1f - buttonXPercentage * 3) / 2f;
		float spacingYPercentage = (1f - buttonYPercentage * 4) / 3f;

		RectTransform panelRect = GetComponent<RectTransform>();
		float realWidth = panelRect.localScale.x * panelRect.rect.width;
		RectTransform buttonRect = buttonPrefab.GetComponent<RectTransform>();
		float xScale = buttonXPercentage * realWidth / buttonRect.rect.width;
		float buttonWidth = buttonRect.rect.width * xScale;
		float stepX = spacingXPercentage * realWidth + buttonWidth;

		float realHeight = panelRect.localScale.y * panelRect.rect.height;
		float yScale = buttonYPercentage * realHeight / buttonRect.rect.height;
		float buttonHeight = buttonRect.rect.height * yScale;
		float stepY = spacingYPercentage * realHeight + buttonHeight;
		Debug.Log(stepY);

		float y = realHeight / 2 - buttonHeight / 2;
		float x = buttonWidth / 2 - realWidth / 2;

		int count = 0;
		for (int i = 0; i < 3; i++)
		{
			for (int j = 0; j < 3; j++)
			{
				GameObject button = Instantiate(buttonPrefab) as GameObject;
				button.transform.SetParent(this.gameObject.transform);
				button.transform.localScale = new Vector3(xScale, yScale, 1);
				button.transform.localPosition = new Vector3(x, y, 0);
				string color = colorList.keys[count];
				string path = string.Format("Colors/ic_{0}_button", color);
				count++;
				button.gameObject.GetComponent<Image>().sprite = Resources.Load<Sprite>(path);
				button.GetComponent<ColorButton>().ColorName = color;
				button.GetComponent<Button> ().onClick.AddListener (OnColorButtonClick);
				x += stepX;
			}
			y -= stepY;
			x = buttonWidth / 2 - realWidth / 2;
		}

		GameObject callButton = Instantiate(callButtonPrefab) as GameObject;
		callButton.transform.SetParent(this.gameObject.transform);
		callButton.transform.localScale = new Vector3(xScale, yScale, 1);
		callButton.transform.localPosition = new Vector3(x, y, 0);
		callButton.gameObject.GetComponent<Image>().sprite = Resources.Load<Sprite>("Colors/ic_call_button");
		x += stepX;

		GameObject pinkButton = Instantiate(buttonPrefab) as GameObject;
		pinkButton.transform.SetParent(this.gameObject.transform);
		pinkButton.transform.localScale = new Vector3(xScale, yScale, 1);
		pinkButton.transform.localPosition = new Vector3(x, y, 0);
		pinkButton.gameObject.GetComponent<Image>().sprite = Resources.Load<Sprite>("Colors/ic_pink_button");
		pinkButton.GetComponent<ColorButton>().ColorName = "pink";
		pinkButton.GetComponent<Button> ().onClick.AddListener (OnColorButtonClick);
		x += stepX;

		GameObject recordButton = Instantiate(recordButtonPrefab) as GameObject;
		recordButton.transform.SetParent(this.gameObject.transform);
		recordButton.transform.localScale = new Vector3(xScale, yScale, 1);
		recordButton.transform.localPosition = new Vector3(x, y, 0);
		recordButton.gameObject.GetComponent<Image>().sprite = Resources.Load<Sprite>("Colors/ic_dictaphone_button");
	}

	public void OnColorButtonClick()
	{
		ColorButton button = EventSystem.current.currentSelectedGameObject.
			GetComponent<ColorButton>();
		string currText = inputField.text;
		inputField.text = strings[button.ColorName];
		inputField.transform.Find("Text").GetComponent<Text>().color = colors[button.ColorName];

		Helper.PlayAudio(button.ColorName, this.gameObject.GetComponent<AudioSource>());
	}

	// Update is called once per frame
	void Update()
	{

	}
}
